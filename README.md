# Request Google JWT

Request an access token from Google Oauth2 using Service account credentials

```bash
$ ./reqOauth.sh -c <credential-json-string> -s <oauth-scope>
```
