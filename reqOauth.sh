#!/bin/bash

helper() {
	echo "$0 Params:"
    echo -e "\t-c | --credential <value>\tService Account credential"
    echo -e "\t-s | --scope <value>\t\tJWT request scope. More info at https://developers.google.com/identity/protocols/oauth2/scopes"
}

# Parsing params
while (( $# )); do
	case $1 in
		-h | --help)
			helper
			exit 0;
		;;
		-c | --credential)
		 	credential=$(jq -c -r . <<< "$2" 2>&1);
			[ $? -ne 0 ] && echo "--credential param seems not to be a valid json. Error: $credential" && exit 1;
			shift;
		;;
		-s | --scope)
			scope="$2"
			shift;
		;;
	esac;
	shift;
done

# download jwt parser
wget -q https://github.com/gasconleon/jwt.sh/raw/master/jwt.sh
chmod +x jwt.sh

# parse data from credentials
token_uri=$(echo $credential | jq -r ".token_uri")
[[ "$token_uri" == "null" ]] && echo "Token URI not found in the credentials." && exit 2
client_email=$(echo $credential | jq -r ".client_email")
[[ "$client_email" == "null" ]] && echo "Service account email does not found in the credentials." && exit 3
echo $credential | jq -r ".private_key" > private_key
[[ "$(cat private_key)" == "null" ]] && echo "Service account email does not found in the credentials." && exit 4
epoch_now=$(date +%s)
epoch_expire=$(($epoch_now + 3600))
payload=$(jq -n --arg iss "$client_email" --arg scope "$scope" --arg aud "$token_uri" --arg exp $epoch_expire --arg iat $epoch_now '{iss:$iss,scope:$scope,aud:$aud,exp:$exp,iat:$iat}')

# generate jwt
jwt=$(./jwt.sh -a RS256 -P private_key -p "$payload")
rm private_key
rm jwt.sh
[ $? -ne 0 ] && exit $?

# request token
curl -s -d "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=${jwt}" "https://oauth2.googleapis.com/token"